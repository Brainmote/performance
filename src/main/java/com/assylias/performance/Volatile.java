/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Benchmark)
public class Volatile {

    private volatile int x = 10;
    private int y = 10;
    private int z = 123;
    private final Random random = new Random();

    @Benchmark
    public void baseline() {
    }

    @Benchmark
    public int baselineRandom() {
        return random.nextInt();
    }
    
    @Benchmark
    public int volatileRead() {
        return x;
    }

    @Benchmark
    public int volatileWrite() {
        x = random.nextInt();
        return x;
    }

    @Benchmark
    public int normalRead() {
        return y;
    }

    @Benchmark
    public int normalWrite() {
        y = random.nextInt();
        return y;
    }
}