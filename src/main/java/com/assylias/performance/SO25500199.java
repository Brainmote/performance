/*
 * Copyright 2014 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 * Find the minimum positive number in an array
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO25500199 {

  @Param({"100000"})
  private int size;
  private int array[];

  @Setup
  public void prepare() {
    Random r = new Random();
    array = new int[size];
    for (int i = 0; i < size; i++) {
      array[i] = r.nextInt();
    }
  }

  @Benchmark public double baseline() {
    return array[0];
  }

  @Benchmark public double manual() {
    boolean max_val_present = false;

    int min = Integer.MAX_VALUE;

    for (int i = 0; i < array.length; i++) // Loop to find the smallest number in array[]
    {
      if (min > array[i] && array[i] > 0)
        min = array[i];
      //Edge Case, if all numbers are negative and a MAX value is present
      if (array[i] == Integer.MAX_VALUE)
        max_val_present = true;
    }

    if (min == Integer.MAX_VALUE && !max_val_present)
      //no positive value found and Integer.MAX_VALUE
      //is also not present, return -1 as indicator
      return -1;

    return min; //return min positive if -1 is not returned
  }

  @Benchmark public int stream() {
    return Arrays.stream(array).filter(i -> i >= 0).min().orElse(-1);
  }

  @Benchmark public int parallel() {
    return Arrays.stream(array).parallel().filter(i -> i >= 0).min().orElse(-1);
  }
}
