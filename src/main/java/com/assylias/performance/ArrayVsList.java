/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Compare basic array operations performance on arrays vs ArrayLists. <br>Results:
 * <pre>
 * {@code Benchmark                              Mode Thr    Cnt  Sec         Mean   Mean error    Units
 * c.a.p.ArrayVsList.createArray1         avgt   1      5    2       12.293        0.867  nsec/op
 * c.a.p.ArrayVsList.createArray10000     avgt   1      5    2      428.369        9.997  nsec/op
 * c.a.p.ArrayVsList.createArray1M        avgt   1      5    2   342972.975     7253.989  nsec/op
 * c.a.p.ArrayVsList.createList1          avgt   1      5    2       15.980        0.962  nsec/op
 * c.a.p.ArrayVsList.createList10000      avgt   1      5    2      445.832        7.042  nsec/op
 * c.a.p.ArrayVsList.createList1M         avgt   1      5    2   340168.305    16531.466  nsec/op
 * c.a.p.ArrayVsList.getArray             avgt   1      5    2        3.313        0.101  nsec/op
 * c.a.p.ArrayVsList.getList              avgt   1      5    2        3.398        0.055  nsec/op
 * c.a.p.ArrayVsList.setArray             avgt   1      5    2        5.425        0.123  nsec/op
 * c.a.p.ArrayVsList.setList              avgt   1      5    2        7.058        0.292  nsec/op}
 * </pre>
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class ArrayVsList {

    private int one = 1;
    private int thousand = 1000;
    private int million = 1000000;
    private Integer value = 1000;
    private Integer[] array = {1, 2, 3, 4};
    private List<Integer> list = Arrays.asList(1, 2, 3, 4);

    @Benchmark
    public Integer[] createArray1() {
        return new Integer[one];
    }

    @Benchmark
    public List<Integer> createList1() {
        return new ArrayList<>(one);
    }

    @Benchmark
    public Integer[] createArray10000() {
        return new Integer[thousand];
    }

    @Benchmark
    public List<Integer> createList10000() {
        return new ArrayList<>(thousand);
    }

    @Benchmark
    public Integer[] createArray1M() {
        return new Integer[million];
    }

    @Benchmark
    public List<Integer> createList1M() {
        return new ArrayList<>(million);
    }

    @Benchmark
    public Integer[] setArray() {
        array[one] = value;
        return array;
    }

    @Benchmark
    public List setList() {
        list.set(one, value);
        return list;
    }

    @Benchmark
    public Integer getArray() {
        return array[one];
    }

    @Benchmark
    public Integer getList() {
        return list.get(one);
    }
}
