/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.ArrayList;
import java.util.LinkedList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO26404256 {

  private int length = 1_000_000;

  @Benchmark public ArrayList<Integer> arrayList() {
    ArrayList<Integer> list = new ArrayList<Integer>();
    for (int i = 0; i < length; i++) {
      list.add(i);
    }
    return list;
  }

  @Benchmark public LinkedList<Integer> linkedList() {
    LinkedList<Integer> list = new LinkedList<Integer>();
    for (int i = 0; i < length; i++) {
      list.add(i);
    }
    return list;
  }
}
