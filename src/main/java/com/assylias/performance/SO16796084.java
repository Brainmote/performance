/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 * @author Yann Le Tallec
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO16796084 {

    private int[] nums = {89023, 180725, 263824, 4657, 1237, 7987523, 654, 92235, 79875, 852};
    private int[] results = {89023, 180725, 263824, 4657, 1237, 7987523, 654, 92235, 79875, 852};
    
    @Benchmark
    public void modulus() {
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            int offset = num % 16;
            int result = offset == 0 ? num : num + 16 - offset;
            results[i] = result;
        }
    }

    @Benchmark
    public void bitOps() {
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            results[i] = (num + 15) & (~15);
        }
    }

    @Benchmark
    public void baseline() {
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            results[i] = num;
        }
    }
}
