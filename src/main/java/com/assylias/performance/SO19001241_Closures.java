/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assylias.performance;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Benchmark                                          Mode Thr    Cnt  Sec         Mean   Mean error    Units
 * c.a.p.SO19001241_Closures.anonymousWithArgs        avgt   1      5    2       11.551        0.303  nsec/op
 * c.a.p.SO19001241_Closures.anonymousWithoutArgs     avgt   1      5    2       11.539        0.288  nsec/op
 * c.a.p.SO19001241_Closures.baseline                 avgt   1      5    2       11.629        0.369  nsec/op
 * c.a.p.SO19001241_Closures.lambdaWithArgs           avgt   1      5    2       11.516        0.210  nsec/op
 * c.a.p.SO19001241_Closures.lambdaWithoutArgs        avgt   1      5    2       11.495        0.095  nsec/op
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO19001241_Closures {
    private final Random rand = new Random();
    private int a = 0;

    static interface CallbackWithArgs {
        int payload(int i);
    }
    static interface CallbackWithoutArgs {
        int payload();
    }

    @Benchmark
    public int baseline() {
        return rand.nextInt();
    }

    @Benchmark
    public int anonymousWithArgs() {
        CallbackWithArgs clb = new CallbackWithArgs() {
            @Override public int payload(int i) {
                return i + rand.nextInt();
            }
        };
        return clb.payload(a);
    }

    @Benchmark
    public int lambdaWithArgs() {
        CallbackWithArgs clb = i -> i + rand.nextInt();
        return clb.payload(a);
    }

    @Benchmark
    public int anonymousWithoutArgs() {
        CallbackWithoutArgs clb = new CallbackWithoutArgs() {
            @Override public int payload() {
                a += rand.nextInt();
                return a;
            }
        };
        return clb.payload();
    }

    @Benchmark
    public int lambdaWithoutArgs() {
        CallbackWithoutArgs clb = () -> { a+= rand.nextInt(); return a; };
        return clb.payload();
    }
}
