/*
 * Copyright (C) 2013 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Assesses the cost of calling a synchronized method from a synchronized method
 *
 * # Running: com.assylias.performance.SO18996783.syncOnce
 * Iteration   1 (5000ms in 1 thread): 21.049 nsec/op
 * Iteration   2 (5000ms in 1 thread): 21.052 nsec/op
 * Iteration   3 (5000ms in 1 thread): 20.959 nsec/op
 * Iteration   4 (5000ms in 1 thread): 20.977 nsec/op
 * Iteration   5 (5000ms in 1 thread): 20.977 nsec/op
 * Run confidence intervals "syncOnce": 95% [20.948, 21.058], 99% [20.912, 21.094]
 *
 * # Running: com.assylias.performance.SO18996783.syncTwice
 * Iteration   1 (5000ms in 1 thread): 21.006 nsec/op
 * Iteration   2 (5000ms in 1 thread): 20.954 nsec/op
 * Iteration   3 (5000ms in 1 thread): 20.953 nsec/op
 * Iteration   4 (5000ms in 1 thread): 20.869 nsec/op
 * Iteration   5 (5000ms in 1 thread): 20.903 nsec/op
 * Run confidence intervals "syncTwice": 95% [20.872, 21.002], 99% [20.829, 21.045]
 *
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO18996783 {

    private int value = 5;

    @Benchmark
    public int syncTwice() {
        return x_sync();
    }

    @Benchmark
    public int syncOnce() {
        return x_no_sync();
    }
    private synchronized int x_sync() {
        return y_sync();
    }

    private synchronized int y_sync() {
        return value;
    }

    private synchronized int x_no_sync() {
        return y_no_sync();
    }

    private int y_no_sync() {
        return value;
    }
}
