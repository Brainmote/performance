/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO26775395 {

  @Param({"10", "1000", "1000000"})
  int n;
  List<Weighter> weights;

  @Setup
  public void setup() {
    weights = new Random().doubles(n)
            .mapToObj(Weighter::new)
            .collect(toList());
  }

  @Benchmark public DoubleSummaryStatistics collector() {
    return weights.stream().collect(Collectors.summarizingDouble(Weighter::w));
  }

  @Benchmark public DoubleSummaryStatistics summary() {
    return weights.stream().mapToDouble(Weighter::w).summaryStatistics();
  }

  public static class Weighter {
    private final double w;

    public Weighter(double w) {
      this.w = w;
    }

    public double w() {
      return w;
    }
  }

}
