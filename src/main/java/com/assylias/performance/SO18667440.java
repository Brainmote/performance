/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.performance;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Iteration 1 (2000ms in 1 thread): 342.787 nsec/op
 * Iteration 2 (2000ms in 1 thread): 336.864 nsec/op
 * Iteration 3 (2000ms in 1 thread): 327.543 nsec/op
 * Iteration 4 (2000ms in 1 thread): 326.594 nsec/op
 * Iteration 5 (2000ms in 1 thread): 325.647 nsec/op
 * <p>
 * Run result "slowItDown": 331.887 ±(95%) 9.402 ±(99%) 15.593 nsec/op
 * Run statistics "slowItDown": min = 325.647, avg = 331.887, max = 342.787, stdev = 7.573
 * Run confidence intervals "slowItDown": 95% [322.485, 341.289], 99% [316.294, 347.480]
 */
@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@BenchmarkMode(Mode.AverageTime)
public class SO18667440 {

    @Benchmark
    public int slowItDown() {
        int result = 0;
        for (int i = 1; i <= 1000; i++) {
            result += i;
        }
        return result;
    }
}
