/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO27167199 {
  private static String[] EMPTY_ARRAY = new String[0];


  @Benchmark public String[] constant() {
    return EMPTY_ARRAY;
  }

  @Benchmark public String[] newArray() {
    return new String[0];
  }
}
