/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO26335959 {

  private String s = "CAST";
  private Object o;

  @Setup public void setup() {
    o = "CAST";
  }

  @Benchmark public String noCast() {
    return s;
  }

  @Benchmark public String cast() {
    return (String) o;
  }
}
