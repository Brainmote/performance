/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */
package com.assylias.performance;

import java.util.Arrays;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 *
 */
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO26504151 {

  @Param({"1", "10", "1000", "1000000"})
  int n;
  char zero = '0';


  @Benchmark public String replace() {
    return new String(new char[n]).replace((char) 0, zero);
  }

  @Benchmark public String fill() {
    char[] c = new char[n];
    Arrays.fill(c, zero);
    return new String(c);
  }

  @Benchmark public String builder() {
    StringBuilder sb = new StringBuilder(n);
    for (int i  = 0; i < n; i++) sb.append(zero);
    return sb.toString();
  }

}
