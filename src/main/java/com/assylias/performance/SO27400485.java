/*
 * Copyright (C) 2014 - present by Yann Le Tallec.
 * Please see distribution for license.
 */

package com.assylias.performance;

import java.util.ArrayList;
import java.util.List;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
public class SO27400485 {
  private static Integer I = 0;
  @Param({"1000", "100000"}) int n;

  @Benchmark public Integer[] array() {
    Integer[] array = new Integer[n];
    for (int i = 0; i < n; i++) array[i] = I;
    return array;
  }

  @Benchmark public List<Integer> list() {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < n; i++) list.add(I);
    return list;
  }
}
