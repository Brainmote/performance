This is a repository of various micro benchmarks that use jmh.

The code of the tests is licensed under the [Apache 2.0 License][1]:

>Copyright 2011, Yann Le Tallec
>
>Licensed under the Apache License, Version 2.0 (the "License");
>you may not use this file except in compliance with the License.
>You may obtain a copy of the License at
>
>[http://www.apache.org/licenses/LICENSE-2.0][1]
>
>Unless required by applicable law or agreed to in writing, software
>distributed under the License is distributed on an "AS IS" BASIS,
>WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>See the License for the specific language governing permissions and
>limitations under the License.

 [1]: http://www.apache.org/licenses/LICENSE-2.0